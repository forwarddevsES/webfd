<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('index');

Auth::routes();

Route::prefix('devs')->group(function () {
    Route::get('/', 'AdminController@index')->name('pca');
    Route::resource('portfolios', 'portfoliosController');
    Route::resource('portfolio', 'PortfolioController');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/payment', 'PaymentController@widget')->name('payment');
Route::get('/payment/process', 'PaymentController@process')->name('process');
