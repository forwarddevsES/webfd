<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
      	<meta charset="UTF-8" />
      	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">
        <meta name="keywords" content="forwarddevs desarrollo web, diseño web buenos aires, diseño web en buenos aires, desarrollo web buenos aires, desarrollo web en buenos aires, marketing digital buenos aires, desarrollo web, programación web, diseño web, diseño web empresarial, diseño de pagina web, soluciones web, diseño de sitio web, ecommerce, tienda virtual, tienda online, marketing digital, agencia digital, publicidad en google, publicidad en facebook, publicidad en internet, publicidad online, social media, agencia desarrollo web, diseñador web, desarrollador web, diseñador web freelance, programador web, posicionamiento web, posicionamiento en buscadores, seo, sem, diseño grafico, diseño de logo">

      	<!-- Open Graph -->
      	<meta property="og:title" content="ForwardDevs | Desarrollo web profesional a medida." />
      	<meta property="og:url" content="https://forwarddevs.com" />
      	<meta property="og:type" content="website" />
      	<meta property="og:image" content="http://www.ingeniovirtual.com/wp-content/uploads/tecnologias-de-desarrollo-web.jpg" />
      	<meta property="og:description" content="ForwardDevs Desarrollo Web | Marketing Digital | Diseño Web Buenos Aires, Argentina." />

      	<!-- Twitter Theme -->
      	<meta name="twitter:widgets:theme" content="light">

      	<!-- Titulo &amp; Favicon -->
      	<title>ForwardDevs | Desarrollo web a medida</title>
      	<link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}">

      	<!-- Font -->
      	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700%7CLato:300,400,700' rel='stylesheet' type='text/css'>

      	<!-- Css -->
      	<link rel="stylesheet" href="{{asset('css/core.min.css')}}" />
      	<link rel="stylesheet" href="{{asset('css/skin.css')}}" />

      	<!--[if lt IE 9]>
          	<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
          <![endif]-->

    </head>
    <body class="shop home-page">

    	<!-- Side Navigation Menu -->
    	<aside class="side-navigation-wrapper enter-right" data-no-scrollbar data-animation="push-in">
    		<div class="side-navigation-scroll-pane">
    			<div class="side-navigation-inner">
    				<div class="side-navigation-header">
    					<div class="navigation-hide side-nav-hide">
    						<a href="#">
    							<span class="icon-cancel medium"></span>
    						</a>
    					</div>
    				</div>
    				<nav class="side-navigation nav-block">
    					<ul>
    						<li>
    							<a href="{{url('/')}}">Inicio</a>

    						</li>

    					</ul>
    				</nav>
    				<nav class="side-navigation nav-block">
    					<ul>
                @auth
                    <li>
                      <a href="#">Panel de Clientes</a>
                    </li>
                @else
                    <li>
                      <a href="#" >Ingreso</a>
                    </li>
                @endauth
    					</ul>
    				</nav>
    				<div class="side-navigation-footer">
    					<p class="copyright no-margin-bottom">&copy; 2017-2018 ForwardDevs.</p>
    				</div>
    			</div>
    		</div>
    	</aside>

    	<div class="wrapper">
    		<div class="wrapper-inner">

    			<!-- Header -->
    			<header class="header header-absolute header-transparent header-fixed-on-mobile" data-helper-in-threshold="200" data-helper-out-threshold="500" data-sticky-threshold="200" data-bkg-threshold="100">
    				<div class="header-inner">
    					<div class="row nav-bar">
    						<div class="column width-12 nav-bar-inner">
    							<div class="logo">
    								<div class="logo-inner">
    									<a href="{{url('/')}}"><img src="images/logo.png" alt="" /></a>
    									<a href="{{url('/')}}"><img src="images/logo-dark.png" alt="" /></a>
    								</div>
    							</div>
    							<nav class="navigation nav-block secondary-navigation nav-right">
    								<ul>
                      @auth
                          <li>
                							<div class="v-align-middle">
                									<a href="#" class="button small rounded no-label-on-mobile no-margin-bottom fade-location">Panel de Clientes</a>
                							</div>
                					</li>
                      @else
                          <li>
              								<div class="v-align-middle">
              										<a href="#" class="button small rounded no-label-on-mobile no-margin-bottom fade-location">Ingreso</a>
              								</div>
              						</li>
                      @endauth
    									<li class="aux-navigation hide">
    										<!-- Aux Navigation -->
    										<a href="#" class="navigation-show side-nav-show nav-icon">
    											<span class="icon-menu"></span>
    										</a>
    									</li>
    								</ul>
    							</nav>
    							<nav class="navigation nav-block primary-navigation nav-right sub-menu-indicator">
    								<ul>
    									<li><a href="{{url('/')}}">Inicio</a></li>
    								</ul>
    							</nav>
    						</div>
    					</div>
    				</div>
    			</header>

    			<div class="content clearfix">

    				<div class="section-block featured-media tm-slider-parallax-container">
    					<div class="tm-slider-container full-width-slider" data-featured-slider data-parallax data-parallax-fade-out data-animation="scaleIn" data-speed="1400" data-scale-under="960">
    						<ul class="tms-slides">
    							<li class="tms-slide" data-image data-as-bkg-image data-force-fit data-overlay-bkg-color="#000000" data-overlay-bkg-opacity="0.6">
    								<div class="tms-content">
    									<div class="tms-content-inner left v-align-middle">
    										<div class="row">
    											<div class="column width-6">
    												<h1 class="tms-caption weight-light color-white mb-5" data-no-scale data-animate-in="preset:slideInUpShort;duration:800ms;delay:400ms;">
    													Desarrollo web a medida
    												</h1>
    											</div>
    										</div>
    										<div class="row">
    											<div class="column width-5">
    												<p class="tms-caption lead color-white mb-40" data-no-scale data-animate-in="preset:slideInUpShort;duration:800ms;delay:100ms;">
    													Soluciones únicas para proyectos únicos.
    												</p>
    												<div class="clear"></div>
    												<a href="#homepages" class="tms-caption scroll-link button rounded medium text-small border-white color-white bkg-hover-theme color-hover-white"
    													data-animate-in="preset:slideInUpShort;duration:800ms;delay:1000ms;"
    													data-no-scale>
    													Nuestros Servicios
    												</a>
    											</div>
    										</div>
    									</div>
    								</div>
    								<img data-src="{{asset('images/slider.jpg')}}" data-retina src="images/blank.png" alt=""/>
    							</li>
    						</ul>
    					</div>
    				</div>
            <div class="section-block bkg-grey-ultralight">
    					<div class="row">
    						<div class="column width-12 center">
    							<h3 class="mb-20">¿Qué tecnologías utilizamos?</h3>
    						</div>
    					</div>
    					<div class="row">
    						<div class="column width-12">
    							<div class="tm-slider-container logo-slider pb-30" data-nav-arrows="false" data-nav-show-on-hover="false" data-nav-dark data-nav-keyboard="false" data-auto-advance data-auto-advance-interval="4000" data-progress-bar="false" data-pause-on-hover="false" data-carousel-visible-slides="5">
    								<ul class="tms-slides">
    									<li class="tms-slide">
    										<div class="tms-content-scalable">
    											<img class="max42" data-src="{{asset('images/bootstrap-5.svg')}}" src="images/blank.png" alt=""/>
    										</div>
    									</li>
    									<li class="tms-slide">
    										<div class="tms-content-scalable">
    											<img class="max42" data-src="{{asset('images/vuejs.svg')}}" src="images/blank.png" alt=""/>
    										</div>
    									</li>
    									<li class="tms-slide">
    										<div class="tms-content-scalable">
    											<img class="max42" data-src="{{asset('images/javascript.png')}}" src="images/blank.png" alt=""/>
    										</div>
    									</li>
    									<li class="tms-slide">
    										<div class="tms-content-scalable">
    											<img class="max42" data-src="{{asset('images/laravel.png')}}" src="images/blank.png" alt=""/>
    										</div>
    									</li>
    									<li class="tms-slide">
    										<div class="tms-content-scalable">
    											<img class="max42" data-src="{{asset('images/css3.png')}}" src="images/blank.png" alt=""/>
    										</div>
    									</li>
    									<li class="tms-slide">
    										<div class="tms-content-scalable">
    											<img class="max42" data-src="{{asset('images/html.png')}}" src="images/blank.png" alt=""/>
    										</div>
    									</li>
                      <li class="tms-slide">
    										<div class="tms-content-scalable">
    											<img class="max42" data-src="{{asset('images/php.png')}}" src="images/blank.png" alt=""/>
    										</div>
    									</li>

    								</ul>
    							</div>
    						</div>
    					</div>
    				</div>
            <div class="section-block pt-0 pb-0">
    					<div class="row">
    						<div class="column width-10 offset-1 center">
    							<h3 class="mb-50"></h3>
    						</div>
    					</div>
    				</div>
            <div class="section-block pt-0 pb-0">
    					<div class="row">
    						<div class="column width-10 offset-1 center">
    							<h3 class="mb-50">¿Qué hacemos?</h3>
    						</div>
    					</div>
    				</div>
    				<div class="section-block pt-0 pb-0 feature-column-group">
    					<div class="row full-width collapse flex boxes">
    						<div class="column width-4 bkg-grey-ultralight">
    							<div class="feature-column large center horizon" data-animate-in="preset:scaleIn;duration:1000ms;" data-threshold="1">
    								<span class="feature-icon icon-browser color-theme"></span>
    								<div class="feature-text">
    									<h4>Diseño Web</h4>
    									<p>Sitios Webs Optimizados. Diseño moderno y atractivo. Compatibles con distintos navegadores y dispositivos. Usabilidad del Usuario.</p>
    								</div>
    							</div>
    						</div>
    						<div class="column width-4 bkg-gradient-royal-garden color-white">
    							<div class="feature-column large center horizon" data-animate-in="preset:scaleIn;duration:1000ms;delay:200ms;" data-threshold="1">
    								<span class="feature-icon icon-code color-theme"></span>
    								<div class="feature-text">
    									<h4>Desarrollo web</h4>
    									<p>Desarrollo de sistemas web a medida. Tus ideas y necesidades materializadas en un Sitio Web. Seguridad informática.</p>
    								</div>
    							</div>
    						</div>
    						<div class="column width-4 bkg-grey-ultralight">
    							<div class="feature-column large center horizon" data-animate-in="preset:scaleIn;duration:1000ms;delay:400ms;" data-threshold="1">
    								<span class="feature-icon icon-globe color-theme"></span>
    								<div class="feature-text">
    									<h4>Posicionamiento Web</h4>
    									<p>SEO. Optimización para los motores de búsqueda. Código limpio y ordenado. Posicionamiento orgánico a través del uso de palabras claves.</p>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div id="homepages" class="section-block pt-20 pb-0 grid-filter-menu bkg-white">
    					<div class="row">
    						<div class="column width-12 left">
    							<div class="filter-menu-inner">
    								<div class="row flex">
    									<div class="column width-4 v-align-middle">
    										<h4>Nuestros Trabajos</h4>
    									</div>
    									<div class="column width-8 right left-on-mobile">
    										<ul>
                          <li><a class="active" href="#" data-filter="*">Todo</a></li>
                          @foreach ($categories as $category)
                            <li><a href="#" data-filter=".{{$category->slug}}">{{$category->name}}</a></li>
                          @endforeach
    										</ul>
    									</div>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="section-block grid-container fade-in-progressively pt-80 bkg-grey-ultralight" data-layout-mode="fitRows" data-animate-filter-duration="700" data-animate-resize data-animate-resize-duration="0">
    					<div class="row">
    						<div class="column width-12">
    							<div class="row grid content-grid-3">
                    @foreach ($portfolios as $portfolio)
                      <div class="grid-item {{$portfolio->category->slug}} one-page-layout">
      									<div class="thumbnail img-scale-in" data-hover-easing="easeInOut" data-hover-speed="700" data-hover-bkg-color="#ffffff" data-hover-bkg-opacity="1">
      										<a class="overlay-link" href="#">
      											<img src="{{asset($portfolio->image->src)}}" alt=""/>
      											<span class="overlay-info">
      												<span>
      													<span>
      														<span class="icon-plus"></span>
      													</span>
      												</span>
      											</span>
      										</a>
      									</div>
      									<div class="item-description with-background">
      										<h3 class="project-title">{{$portfolio->title}}</h3>
      									</div>
      								</div>
                    @endforeach

    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    			<footer class="footer footer-light with-border">

    				<div class="footer-bottom">
    					<div class="row">
    						<div class="column width-12">
    							<div class="footer-bottom-inner">
    								<p class="copyright pull-left clear-float-on-mobile">&copy; ForwardDevs. All Rights Reserved.</p> <a href="#" class="scroll-to-top pull-right clear-on-mobile" data-no-hide>Volver arriba</a>
    							</div>
    						</div>
    					</div>
    				</div>
    			</footer>
    		</div>
    	</div>
    	<script src="js/jquery-3.2.1.min.js"></script>
    	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3JCAhNj6tVAO_LSb8M-AzMlidiT-RPAs"></script>
    	<script src="js/timber.master.min.js"></script>
    </body>
</html>
