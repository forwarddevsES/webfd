@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Resumen</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @php
                      $pingbacks = App\Pingback::all();
                    @endphp
                    @foreach ($pingbacks as $pb)
                      {{$pb->user->name}} - {{$pb->goodsid}} (Estado : {{$pb->status}}) #{{$pb->sig}} <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
