@extends('plantilla.admin')
@section('content')

  <div class="row">

    <div class="col-xl-6">

      <div class="m-portlet m-portlet--full-height ">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">
                Ultimos Tickets
              </h3>
            </div>
          </div>

        </div>
        <div class="m-portlet__body">
          <div class="m-widget3">
            @php
              $tickets = Ticket::where('user_id', Auth::user()->id)->take(3)->get();
            @endphp
            @foreach ($tickets as $ticket)
              <div class="m-widget3__item">
                <div class="m-widget3__header">
                  <div class="m-widget3__user-img">
                    <img class="m-widget3__img" src="{{asset($ticket->user->avatar)}}" alt="">
                  </div>
                  <div class="m-widget3__info">
                    <span class="m-widget3__username">
                      {{$ticket->user->name}}
                    </span>
                    <br>
                    <span class="m-widget3__time">
                      {{Carbon::createFromTimeStamp(strtotime($ticket->created_at))->diffForHumans()}}
                    </span>
                  </div>
                  @if ($ticket->estado == 0)
                    <span class="m-widget3__status m--font-success"><a href="{{route('tickets.show', ['id' => $ticket->id])}}" style="text-decoration:none;">{{__('web.abierto')}}</a></span>
                  @else
                    <span class="m-widget3__status m--font-danger"><a href="{{route('tickets.show', ['id' => $ticket->id])}}" style="text-decoration:none;">{{__('web.cerrado')}}</a></span>
                  @endif
                </div>
                <div class="m-widget3__body">
                  <p class="m-widget3__text">
                    {{$ticket->ticket}}
                  </p>
                </div>
              </div>
            @endforeach



          </div>
        </div>
      </div>

    </div>

  </div>

@endsection
