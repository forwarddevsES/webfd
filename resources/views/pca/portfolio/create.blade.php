@extends('layouts.admin')

@section('content')
  <div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">Nuevo Portfolio</h3>
        </div>
      </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" action="{{route('portfolio.store')}}"  method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      {{ method_field('POST') }}
      <div class="m-portlet__body">
        <div class="form-group m-form__group">
          <label for="portada">
            Foto del proyecto
          </label>
          <div></div>
          <label class="custom-file">
            <input type="file" name="image" placeholder="Haga click para seleccionar un archivo" class="form-control-file">
          </label>
        </div>
        <div class="form-group m-form__group">
          <label for="title">Titulo</label>
          <input class="form-control m-input" type="text" name="title" value="">
          <span class="m-form__help">Escriba el titulo del proyecto.</span>
        </div>
        <div class="form-group m-form__group">
          <label for="description">Descripción</label>
          <textarea class="form-control m-textarea" name="description" rows="8" cols="80"></textarea>
          <span class="m-form__help">Escriba la descripción del proyecto.</span>
        </div>
        <div class="form-group m-form__group">
          <label for="owner">Dueño/Propietario</label>
          <input class="form-control m-input" type="text" name="owner" value="">
          <span class="m-form__help">Escriba el nombre del propietario del proyecto.</span>
        </div>
        <div class="form-group m-form__group">
          <label for="developer_id">Categoría</label>
          <select class="form-control m-select" name="category_id">
            @foreach ($categories as $category)
              <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
          </select>
          <span class="m-form__help">Seleccione la categoría del proyecto.</span>
        </div>
        <div class="form-group m-form__group">
          <label for="developer_id">Desarrollador</label>
          <select class="form-control m-select" name="developer_id">
            @foreach ($developers as $developer)
              <option value="{{$developer->id}}">{{$developer->alias}}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
          <button type="submit" class="btn btn-primary">Crear Portfolio</button>
        </div>
      </div>
    </form>
  </div>
@endsection
@section('scripts')
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=1ctd9ar2p0gqkcate7k6i1cyijcegdgbgfb4lw3gjth7ff32"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection
