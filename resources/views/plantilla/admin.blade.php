
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
	<head>
		<meta charset="utf-8" />
		<title>
			ForwardDevs - PCA
		</title>
    <meta name="description" content="ForwardDevs Desarrollo Web | Marketing Digital | Diseño Web Buenos Aires, Argentina.">
		<meta name="keywords" content="forwarddevs desarrollo web, diseño web buenos aires, diseño web en buenos aires, desarrollo web buenos aires, desarrollo web en buenos aires, marketing digital buenos aires, desarrollo web, programación web, diseño web, diseño web empresarial, diseño de pagina web, soluciones web, diseño de sitio web, ecommerce, tienda virtual, tienda online, marketing digital, agencia digital, publicidad en google, publicidad en facebook, publicidad en internet, publicidad online, social media, agencia desarrollo web, diseñador web, desarrollador web, diseñador web freelance, programador web, posicionamiento web, posicionamiento en buscadores, seo, sem, diseño grafico, diseño de logo">

		<!-- Open Graph -->
		<meta property="og:title" content="ForwardDevs | Desarrollo web profesional a medida." />
		<meta property="og:url" content="https://forwarddevs.com" />
		<meta property="og:type" content="website" />
		<meta property="og:image" content="http://www.ingeniovirtual.com/wp-content/uploads/tecnologias-de-desarrollo-web.jpg" />
		<meta property="og:description" content="ForwardDevs Desarrollo Web | Marketing Digital | Diseño Web Buenos Aires, Argentina." />

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		{{-- Fuente de la web --}}
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		{{-- Fin:Fuente de la web --}}
    {{-- Estilos --}}
		{{-- Base --}}
		<link href="{{asset('pca/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('pca/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />

		@yield('styles')
    {{-- Icono --}}
		<link rel="shortcut icon" href="{{asset('favicon.ico')}}" />
	</head>

	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

		<div class="m-grid m-grid--hor m-grid--root m-page">

			<header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200" >
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">
						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand  m-brand--skin-dark ">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="index.html" class="m-brand__logo-wrapper">
										<img alt="" style="width:100px; height:60px;" src="{{asset('images/logo1.png')}}"/>
									</a>
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">

									<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block
					 ">
										<span></span>
									</a>

									<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>

								</div>
							</div>
						</div>
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
								<i class="la la-close"></i>
							</button>

							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">

										@include('pca.ext.perfil')

									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
					<i class="la la-close"></i>
				</button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
	<div
		id="m_ver_menu"
		class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
		data-menu-vertical="true"
		 data-menu-scrollable="false" data-menu-dropdown-timeout="500"
		>
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
								<a  href="{{url('/devs')}}" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-list-3"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												Resumen
											</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									Modulos
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>



						</ul>
					</div>
				</div>
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">
									Panel Administrativo
								</h3>
							</div>

						</div>
					</div>
					<div class="m-content">
						@yield('content')
					</div>
				</div>
			</div>
			@include('pca.ext.footer')
		</div>

		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>
		<script src="{{asset('pca/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('pca/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>

		<script src="{{asset('pca/app/js/dashboard.js')}}" type="text/javascript"></script>

		@yield('scripts')
	</body>
</html>
