<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portfolio;
use App\PortfolioImage;
use App\Developer;
use App\Category;
use Storage;


class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $portfolios = Portfolio::all();
      return view('pca.portfolio.index', compact('portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $developers = Developer::all();
        $categories = Category::all();
        return view('pca.portfolio.create', compact('developers', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path= $request->file('image')->store('portfolio', 'public');
        $url = Storage::url($path);
        $portfolio = new Portfolio();
        $portfolio->title = $request->title;
        $portfolio->description = $request->description;
        $portfolio->owner = $request->owner;
        $portfolio->developer_id = $request->developer_id;
        $portfolio->category_id = $request->category_id;
        $portfolio->save();
        $portfolioimage = new PortfolioImage();
        $portfolioimage->portfolio_id = $portfolio->id;
        $portfolioimage->src = $url;
        $portfolioimage->save();
        return redirect('devs/portfolio')->with('message', 'Se ha creado el portfolio correctamente haga <a href="'.url("/").'">click aquí</a> para verlo.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
