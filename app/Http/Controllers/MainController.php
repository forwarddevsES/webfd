<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Portfolio;

class MainController extends Controller
{
    public function index()
    {
      $categories = Category::all();
      $portfolios = Portfolio::all();
      return view('welcome', compact('categories', 'portfolios'));
    }
}
