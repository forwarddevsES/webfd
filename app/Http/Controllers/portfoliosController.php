<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Portfolio;
use Auth;
use Carbon\Carbon;

class portfoliosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('pca.portfolios.inicio');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pca.portfolios.nuevo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Portfolio::create($request->all());

        return redirect('pca/portfolios')->with('message', 'Create un Portfolio.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $modelo = Portfolio::find($id);
        return view('pca.portfolios.ver', compact('modelo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modelo = Portfolio::find($id);
        return view('pca.portfolios.editar', compact('modelo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $modelo = Portfolio::find($id);
        $modelo->updated_at = Carbon::now()->toDateTimeString();
        $modelo->save();
        $modelo->update($request->all());
        return redirect('pca/portfolios')->with('message', 'Editaste un Portfolio #'.$modelo->id.'.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $modelo = Portfolio::find($id);

        $modelo->delete();
        return redirect('pca/portfolios')->with('message', 'Borraste el Portfolio #'.$id.'.');
    }
}
