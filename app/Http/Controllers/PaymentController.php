<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Paymentwall;
use Paymentwall_Config;
use Paymentwall_Widget;
use Auth;
use Paymentwall_Pingback;
use Paymentwall_Product;
use Paymentwall_Base;
use App\Pingback;

class PaymentController extends Controller
{


    public function widget()
    {
        Paymentwall_Config::getInstance()->set(array(
          'api_type' => Paymentwall_Config::API_GOODS,
          'public_key' => 'cec62f57aed3d8c44894b1f6638c6e43',
          'private_key' => 'c749a526b9ee5f1532bd6da853da857c'
        ));

        $widget = new Paymentwall_Widget(
          Auth::user()->id, // uid
          'p1_1', // widget
          array(
            new Paymentwall_Product(
        			'product301',                           // id of the product in your system
        			9.99,                                   // price
        			'USD',                                  // currency code
        			'Gold Membership',                      // product name
        			Paymentwall_Product::TYPE_FIXED // this is a time-based product; for one-time products, use Paymentwall_Product::TYPE_FIXED and omit the following 3 array element
        		)
          ), // Product parts, leave empty for Widget API
          array(
              'email' => Auth::user()->email,
              'timestamp' => 'transaction_current_timestamp'
          )
        );
        return view('payment.e1', compact('widget'));
    }
    public function process()
    {
      Paymentwall_Base::setApiType(Paymentwall_Base::API_GOODS);
      Paymentwall_Base::setAppKey('cec62f57aed3d8c44894b1f6638c6e43'); // available in your Paymentwall merchant area
      Paymentwall_Base::setSecretKey('c749a526b9ee5f1532bd6da853da857c'); // available in your Paymentwall merchant area

      $pingback = new Paymentwall_Pingback($_GET, $_SERVER['REMOTE_ADDR']);
      if ($pingback->validate()) {
          $productId = $pingback->getProduct()->getId();
          $status = "0";
          if ($pingback->isDeliverable()) {
              $status = "1";
          } else if ($pingback->isCancelable()) {
              $status = "2";
          } else if ($pingback->isUnderReview()) {
              $status = "3";
          }
          $pb = new Pingback();
          $pb->user_id = $pingback->getParameter('uid');
          $pb->goodsid = $pingback->getParameter('goodsid');
          $pb->is_test = $pingback->getParameter('is_test');
          $pb->slength = $pingback->getParameter('slength');
          $pb->speriod = $pingback->getParameter('speriod');
          $pb->ref = $pingback->getParameter('ref');
          $pb->status = $status;
          $pb->sig = $pingback->getParameter('sig');
          if ($pb->save()) {
            echo 'OK';
          }
      } else {
          echo $pingback->getErrorSummary();
      }
    }
}
