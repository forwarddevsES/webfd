<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Problema extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
    protected $fillable = ['problema'];
}
