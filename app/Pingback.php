<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pingback extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'goodsid', 'is_test', 'ref', 'speriod', 'slength', 'sig', 'currency', 'status'];

    /**
     * Return the User model
     *
     * @return App\User
     */
    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
