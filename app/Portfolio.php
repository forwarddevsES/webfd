<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'owner', 'developer_id', 'category_id'];
    public function category()
    {
      return $this->belongsTo('App\Category');
    }
    public function developer()
    {
      return $this->belongsTo('App\Developer');
    }
    public function image()
    {
        return $this->hasOne('App\PortfolioImage');
    }
}
